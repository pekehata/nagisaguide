package com.nagisapark.nagisaguide;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.airbnb.epoxy.EpoxyRecyclerView;
import com.nagisapark.nagisaguide.JsonGsonConverter.*;
import com.nagisapark.nagisaguide.databinding.FragmentSpeciesSelectionBinding;

public class SpeciesSelectionFragment extends Fragment {

    int drawableIds[] = {
            R.drawable.button_pink,
            R.drawable.button_blue,
            R.drawable.button_green,
            R.drawable.button_yellow
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return FragmentSpeciesSelectionBinding
                .inflate(inflater)
                .getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //epoxy(RecyclerViewのボイラープレートを減らすライブラリ)の各種設定
        SpeciesSelectionGridController controller = new SpeciesSelectionGridController();
        EpoxyRecyclerView erv = (EpoxyRecyclerView)view.findViewById(R.id.species_selection_grid);
        erv.setAdapter(controller.getAdapter());
        controller.setData(drawableIds, buildOnClickListener());
    }

    private View.OnClickListener buildOnClickListener() {
        return v -> {
            int drawableId = Integer.parseInt((String) v.getTag());
            NavDirections navDirection = SpeciesSelectionFragmentDirections
                    .actionSpeciesSelectionFragmentToNumberSelectionFragment(drawableId);
            Navigation.findNavController(v)
                    .navigate(navDirection);
        };
    }
}