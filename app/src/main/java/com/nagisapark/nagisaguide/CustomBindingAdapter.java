package com.nagisapark.nagisaguide;

import android.widget.ImageView;

import androidx.databinding.BindingAdapter;

public class CustomBindingAdapter {
    //xmlでImageViewにandroid:srcを設定しても動かないので、srcCompatを定義
    @BindingAdapter("srcCompat")
    public static void setSrcCompat(ImageView imageView, int resourceId) {
        imageView.setImageResource(resourceId);
    }
}
