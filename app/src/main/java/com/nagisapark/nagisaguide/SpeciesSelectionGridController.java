package com.nagisapark.nagisaguide;

import android.view.View;

import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import com.airbnb.epoxy.Typed2EpoxyController;

public class SpeciesSelectionGridController extends Typed2EpoxyController<int[], View.OnClickListener> {
    @Override
    protected void buildModels(final int[] drawableIds, View.OnClickListener listener) {
        for(final Integer drawableId : drawableIds){
            new ButtonSpeciesSelectionBindingModel_()
                    .id("ImageButton" + drawableId.toString())
                    .drawableId(drawableId)
                    .onClickSpeciesListener(listener)
                    .addTo(this);
        }
    }
}
