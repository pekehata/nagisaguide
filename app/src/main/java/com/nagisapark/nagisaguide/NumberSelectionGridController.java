package com.nagisapark.nagisaguide;

import android.graphics.drawable.Drawable;
import android.view.View;
import com.airbnb.epoxy.Typed3EpoxyController;

public class NumberSelectionGridController extends Typed3EpoxyController<Drawable, Integer[], View.OnClickListener> {
    @Override
    protected void buildModels(Drawable shape, Integer[] numbers, View.OnClickListener listener) {
        for(final Integer number : numbers) {
            new ButtonNumberSelectionBindingModel_()
                    .id(number)
                    .shape(shape)
                    .number(number)
                    .onClickNumbersListener(listener)
                    .addTo(this);
        }
    }
}
