package com.nagisapark.nagisaguide;

import androidx.activity.OnBackPressedCallback;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.nagisapark.nagisaguide.databinding.ActivityMainBinding;
import java.io.File;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private final String JSON_FILE_NAME = "data.json";
    private final String AUDIO_DIRECTORY = "audio";
    private final String AUDIO_EXTENSION = ".mp3";
    private ActivityMainBinding binding;
    private Toast toast;
    private File jsonFile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //ユーザー認証とファイルダウンロードを実行
        authThenDownload();

        //最初の画面でバックボタンによる前画面への遷移を無効にするため、空のhandleOnBackPressed()を定義
        getOnBackPressedDispatcher().addCallback(new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {}
        });

        //Splashのテーマから切り替える
        setTheme(R.style.AppTheme);

        //デフォルトのsetContentView(R.layout.activity_main)をDataBindingUtilで置き換え、データをセット
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        binding.setVisible(true);  //falseになるまでボタン操作を阻止する
    }

    private void authThenDownload() {
        toast = Toast.makeText(getApplicationContext(),R.string.download_started_message,Toast.LENGTH_LONG);
        toast.show();

        //条件分岐の各所で参照するので、jsonFileを先に初期化しておく
        jsonFile = new File(getFilesDir(),JSON_FILE_NAME);

        //Firebase Authorizationで、匿名認証する
        // ※ Authはネットワークが繋がらないとすぐにonFailureに移るので、timeoutの設定必要なし
        FirebaseAuth.getInstance().signInAnonymously()
                .addOnSuccessListener(authSucceededTaskSnapshot -> {
                    Log.d("DEBUG", "Authentication succeeded.");
                    downloadJsonThenAudio();
                }).addOnFailureListener(authFailedTaskSnapshot -> {
                    Log.d("DEBUG", "Failed to authenticate through Firebase auth...");
                    tryToLoadExistingJson();
        });
    }

    private void downloadJsonThenAudio() {
        //Firebase Storageのセットアップ
        FirebaseStorage storage = FirebaseStorage.getInstance();
        storage.setMaxDownloadRetryTimeMillis(2L);
        StorageReference jsonRef = storage.getReference().child(JSON_FILE_NAME);

        //jsonファイルのダウンロードを実行
        jsonRef.getFile(jsonFile)
                .addOnSuccessListener(this, jsonDownloadTaskSnapshot -> {
                    Log.d("DEBUG", "Successfully downloaded JSON file: " + jsonFile.getName());

                    //jsonデータの読み込み用クラスを初期化
                    JsonGsonConverter.buildJsonData(jsonFile);

                    //オーディオダウンロードの処理
                    downloadAudio();
                })
                .addOnFailureListener(this, jsonDownloadTaskSnapshot -> {
                    Log.d("DEBUG", "Failed to download JSON file: " + jsonFile.getName());
                    tryToLoadExistingJson();
                });
    }

    private void downloadAudio() {
        //音声データ用ディレクトリの作成
        File audioDir = new File(getFilesDir().getPath() + "/" + AUDIO_DIRECTORY);
        audioDir.mkdir();

        //jsonのダウンロードが完了した時のみaudioのダウンロードを実行
        List<String> audioList = JsonGsonConverter.getAudioList();
        for(String audio : audioList) {
            String fileName = audio + AUDIO_EXTENSION;
            File audioFile = new File(getFilesDir(),fileName);
            //ファイルが存在する場合はダウンロードをスキップする
            if(audioFile.exists()) {
                Log.d("DEBUG","Skipped downloading AUDIO file : " + fileName);
                continue;
            }

            //ダウンロードのタイムアウト秒数を設定
            FirebaseStorage storage = FirebaseStorage.getInstance();
            storage.setMaxOperationRetryTimeMillis(3L);

            //オーディオファイルのダウンロード
            StorageReference audioRef = storage.getReference().child(AUDIO_DIRECTORY).child(fileName);
            audioRef.getFile(audioFile)
                    .addOnSuccessListener(this, audioDownloadTaskSnapshot ->
                        Log.d("DEBUG", "Successfully downloaded an audio file : " + audioFile.getName())
                    ).addOnFailureListener(this, audioDownloadTaskSnapshot ->
                        Log.d("DEBUG", "Failed to download an audio file : " + audioFile.getName())
                    );
        }

        toast.setText(R.string.download_completed_message);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.show();

        //最後に必ず透明なViewをfalse=GONEにすること(でないとボタンが操作できない)
        binding.setVisible(false);
    }

    private void tryToLoadExistingJson() {
        //Downloadに失敗しても、既存のファイルがあればそれを使う
        if(jsonFile != null && jsonFile.exists()) {
            Log.d("DEBUG", "Reuse the previously downloaded json file : " + jsonFile.getName());
            JsonGsonConverter.buildJsonData(jsonFile);

            //最後に必ず透明なViewをfalse=GONEにすること(でないとボタンが操作できない)
            binding.setVisible(false);
        } else {
            //なければToastを表示してfinish()
            Log.d("DEBUG", "Exiting app....");
            toast.setText(R.string.app_closed_message);
            toast.show();
            this.finish();
        }
    }
}
