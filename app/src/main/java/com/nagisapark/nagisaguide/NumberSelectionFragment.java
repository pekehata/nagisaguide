package com.nagisapark.nagisaguide;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import com.airbnb.epoxy.EpoxyRecyclerView;
import com.nagisapark.nagisaguide.databinding.FragmentNumberSelectionBinding;
import com.nagisapark.nagisaguide.JsonGsonConverter.*;
import org.jetbrains.annotations.NotNull;

public class NumberSelectionFragment extends Fragment {
    @DrawableRes private int backgroundId;
    @DrawableRes private int speciesLogoId;
    private Integer[] numbers;
    private Drawable buttonShape;
    private FragmentNumberSelectionBinding binding;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //描画に必要なパラメータを決定
        determineValues();
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //DataBindingを利用したパラメータの設定
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_number_selection, container, false);
        binding.setBackgroundId(backgroundId);
        binding.setSpeciesLogoId(speciesLogoId);
        binding.setVisible(true); //一旦透明なViewを最前面レイヤーに表示させて、ボタンを押せなくする
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //epoxy(RecyclerViewのボイラープレートを減らすライブラリ)の各種設定
        NumberSelectionGridController controller = new NumberSelectionGridController();
        EpoxyRecyclerView erv = view.findViewById(R.id.number_selection_grid);
        //アダプターを設定(epoxyが自動生成してくれる)
        erv.setAdapter(controller.getAdapter());
        //controllerにデータを渡して完成
        controller.setData(buttonShape, numbers, buildOnClickListener());

        //onCreateViewで表示した透明なViewを非表示にして、ボタンが押せるようにする
        new Handler().postDelayed(() -> binding.setVisible(false), 300);
    }

    private void determineValues() {
        //SafeArgsを用いたデータの受け取り。
        //前画面のfragmentから渡されたargumentsから、クリックした種族を表す画像のResource IDを取得
        assert getArguments() != null;
        @DrawableRes int selectedSpecies = NumberSelectionFragmentArgs.fromBundle(getArguments()).getSelectedSpecies();

        //前画面で押したボタンから、この画面を表示するのに必要な値を判別
        switch(selectedSpecies) {
            case R.drawable.button_pink :
                backgroundId = R.drawable.back_pink;
                speciesLogoId = R.drawable.symbol_pink;
                buttonShape = requireContext().getDrawable(R.drawable.shape_number_pink);
                numbers = JsonGsonConverter.getInfoCount(SpeciesNames.FISH);
                break;
            case R.drawable.button_blue :
                backgroundId = R.drawable.back_blue;
                speciesLogoId = R.drawable.symbol_blue;
                buttonShape = requireContext().getDrawable(R.drawable.shape_number_blue);
                numbers = JsonGsonConverter.getInfoCount(SpeciesNames.JELLYFISH);
                break;
            case R.drawable.button_green :
                backgroundId = R.drawable.back_green;
                speciesLogoId = R.drawable.symbol_green;
                buttonShape = requireContext().getDrawable(R.drawable.shape_number_green);
                numbers = JsonGsonConverter.getInfoCount(SpeciesNames.CRUSTACEAN);
                break;
            case R.drawable.button_yellow :
                backgroundId = R.drawable.back_yellow;
                speciesLogoId = R.drawable.symbol_yellow;
                buttonShape = requireContext().getDrawable(R.drawable.shape_number_yellow);
                numbers = JsonGsonConverter.getInfoCount(SpeciesNames.SHELLFISH);
                break;
        }
    }

    private View.OnClickListener buildOnClickListener() {
        //RecyclerViewに埋め込まれるボタン用のonClickListenerを返す
        return v -> {
            //ボタンのTagからボタンの番号を取得
            int buttonNumber = Integer.parseInt((String) v.getTag());
            //次のFragmentに番号と背景画像の@DrawableResを渡す
            Navigation.findNavController(v)
                    .navigate(NumberSelectionFragmentDirections.actionNumberSelectionFragmentToPlayerFragment(buttonNumber, backgroundId));
        };
    }
}