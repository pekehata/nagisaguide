package com.nagisapark.nagisaguide;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import com.nagisapark.nagisaguide.databinding.FragmentPlayerBinding;
import com.nagisapark.nagisaguide.JsonGsonConverter.*;
import org.jetbrains.annotations.NotNull;
import java.io.File;
import java.util.Optional;

public class PlayerFragment extends Fragment {
    private final String KEEPER_PREFIX = "keeper_";
    private final String AUDIO_EXTENSION = ".mp3";

    @DrawableRes private int backgroundId;
    @DrawableRes private int keeperPic;

    private FragmentPlayerBinding binding;
    private MediaPlayer mediaPlayer;
    private SeekBar seekBar;
    private File audio;
    private static boolean paused;

    //ThreadからMessageを受け取るためのHandlerを初期化
    private final Handler handler = new Handler(msg -> {
        //onPauseでmediaPlayer.release()した後、mediaPlayer.hoge()しようとするとクラッシュする問題を回避
        if( paused ) return true;

        int currentPosition = mediaPlayer.getCurrentPosition();
        seekBar.setProgress(currentPosition);
        return true;
    });

    //sleep()は別スレッドでないとUIがハングするので、new Thread()する
    private Thread thread = new Thread(() -> {
        paused = false;
        while( ! paused) {
            try {
                //obtain()した方がnew Message()するより効率的らしい
                Message msg = Message.obtain();
                //内容のないmsgが定期的にhandlerに送られればOK
                handler.sendMessage(msg);
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    });

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //描画に必要なパラメータを決定
        determineValues();
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //DataBindingを利用したパラメータの設定
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_player, container, false);
        binding.setVisible(true); //一旦透明なViewを最前面レイヤーに表示させて、ボタンを押せなくする
        binding.setKeeperPicId(keeperPic);
        binding.setBackgroundId(backgroundId);
        binding.setOnPlayListener(v -> mediaPlayer.start());
        binding.setOnStopListener(v -> mediaPlayer.pause());
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //MediaPlayerの設定
        mediaPlayer = MediaPlayer.create(getContext(), Uri.fromFile(audio));

        //SeekBarの設定
        seekBar = view.findViewById(R.id.seek_bar);
        seekBar.setMax(mediaPlayer.getDuration()); //最大値(Max)と現在の値を比べてドットを動かすらしい
        //SeekBarを直接操作した時の処理を定義
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener(){
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if(fromUser) mediaPlayer.seekTo(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });

        //seekBarの同期処理開始
        thread.start();

        //onCreateViewで表示した透明なViewを非表示にして、ボタンが押せるようにする
        handler.postDelayed(() -> binding.setVisible(false), 300);
    }

    @Override
    public void onPause() {
        super.onPause();
        mediaPlayer.pause();
        mediaPlayer.release(); //他のFragmentに遷移してもリソースが保持されている、みたいなことを防ぐため
        paused = true;         //別Thread内のwhileループを止める
    }

    private void determineValues() {
        //SafeArgsを用いたデータの受け取り
        assert getArguments() != null;
        backgroundId = PlayerFragmentArgs.fromBundle(getArguments()).getBackgroundId();
        int number = PlayerFragmentArgs.fromBundle(getArguments()).getNumber();

        //Infoがnullだとヌルポで落ちるのでOptionalにする
        Optional<Info> optionalInfo = Optional.empty();

        //背景画像のリソースIDからSpeciesNameを判別
        switch (backgroundId) {
            case R.drawable.back_pink :
                optionalInfo = JsonGsonConverter.getInfo(SpeciesNames.FISH, number);
                break;
            case R.drawable.back_blue :
                optionalInfo = JsonGsonConverter.getInfo(SpeciesNames.JELLYFISH, number);
                break;
            case R.drawable.back_green :
                optionalInfo = JsonGsonConverter.getInfo(SpeciesNames.CRUSTACEAN, number);
                break;
            case R.drawable.back_yellow :
                optionalInfo = JsonGsonConverter.getInfo(SpeciesNames.SHELLFISH, number);
                break;
        }

        //optionalInfoをunwrapしてinfoに変換
        Info info;
        if( ! optionalInfo.isPresent()){
            info = new Info();
            Log.d("ERROR", "\n     ##### Info has not been loaded.");
        } else
            info = optionalInfo.get();

        //音声ファイルを取得
        audio = new File(requireContext().getFilesDir().getPath()
                        + "/"
                        + info.getAudioFileName()
                        + AUDIO_EXTENSION);

        //飼育員の写真をvoiceOfから判定
        keeperPic = getResources().getIdentifier(KEEPER_PREFIX + info.getVoiceOf(), "drawable", getContext().getPackageName());
    }
}