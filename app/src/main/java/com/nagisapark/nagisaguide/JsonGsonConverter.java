package com.nagisapark.nagisaguide;

import android.annotation.SuppressLint;
import android.util.Log;
import com.google.gson.stream.JsonReader;
import com.google.gson.Gson;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.json.JSONException;
import org.json.JSONObject;


public class JsonGsonConverter {
    private static JsonData jsonData;

    public static JsonData buildJsonData(File jsonFile) {
        try {
            InputStream is = new FileInputStream(jsonFile);
            InputStreamReader isr = new InputStreamReader(is);
            JsonReader jsonReader = new JsonReader(isr);
            jsonData = new Gson().fromJson(jsonReader, JsonData.class);
            System.out.println(new JSONObject(new Gson().toJson(jsonData)).toString(4));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonData;
    }

    public static Integer[] getInfoCount(SpeciesNames speciesName) {
        for (Species species : jsonData.getSpeciesList()) {
            if( ! species.species_name.equals(speciesName.getName()) ) continue;

            int size = species.getInfoList().size();
            Integer[] numbers = new Integer[size];
            for (int i = 0 ; i < size ; i++) {
                numbers[i] = i+1;
            }
            return numbers;
        }
        return new Integer[0];
    }

    @SuppressLint("NewApi")
    public static Optional<Info> getInfo(SpeciesNames species_name, int number) {
        List<Species> speciesList = jsonData.getSpeciesList();
        for (Species species : speciesList) {
            if( ! species.getSpeciesName().equals(species_name.getName())) continue;
            for(Info info : species.getInfoList()) {
                if( info.getNumber().intValue() != number) continue;
                Log.d("TEST", "###### info = " + info.creature_name);
                return Optional.of(info);
            }
        }
        return Optional.empty();
    }

    public static List<String> getAudioList() {
        List<String> audioList = new ArrayList<String>();
        List<Species> speciesList = jsonData.getSpeciesList();
        for (Species species : speciesList) {
            for(Info info : species.getInfoList()) {
                audioList.add(info.getAudioFileName());
            }
        }
        return audioList;
    }

    //  !!!注意!!!
    //  以下の各内部クラスのメンバ変数名は、json内のkeyと一致している必要あり。
    //  もし変更したい場合は、@SerializedNameかsetFieldNamingPolicy()すること。
    public class JsonData {
        private List<Species> species;

        public List<Species> getSpeciesList(){ return species; }
    }
    public class Species {
        private String species_name;
        private List<Info> info;

        public String getSpeciesName(){ return species_name; }
        public List<Info> getInfoList(){ return info; }
    }
    public static class Info {
        private Integer number;
        private String creature_name;
        private String audio_file_name;
        private String voice_of;

        //infoがnullだった時のダミー作成用
        public Info() {
            number = 1;
            creature_name = "";
            audio_file_name = "";
            voice_of = "";
        }

        public Integer getNumber(){ return number; }
        public String getCreatureName(){ return creature_name; }
        public String getAudioFileName(){ return audio_file_name; }
        public String getVoiceOf(){ return voice_of; }
    }

    public enum SpeciesNames {
        FISH("fish"),
        SHELLFISH("shellfish"),
        CRUSTACEAN("crustacean"),
        JELLYFISH("jellyfish");

        private final String name;

        SpeciesNames(String name){
            this.name = name;
        }
        public String getName() { return name;}
    }
}

